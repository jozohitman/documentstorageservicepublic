﻿using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace DocumentStorageService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DocumentsController : ControllerBase
    {
        
        private readonly ILogger<DocumentsController> _logger;

        private IStorage storage;

        public DocumentsController(ILogger<DocumentsController> logger, IStorage pStorage)
        {
            _logger = logger;
            // initialize storage
            storage = pStorage;
            _logger.LogInformation("Storage initialized");
        }


        /// <summary>
        /// method load document from storage and conver it to format specified in Request.Headers["Accept"]
        /// </summary>
        /// <param name="identifier">ID of document</param>
        /// <returns></returns>
        [HttpGet("{identifier}")]
        public async Task<ContentResult> GetAsync(string identifier)
        {
            string type = Request.Headers["Accept"].ToString();
            string stringContent = string.Empty;
            ContentResult contResult;
            try
            {
                if (storage != null)
                {
                    string json = await storage.LoadAsync(identifier);
                    if (json != string.Empty)
                    {
                        stringContent = GetRightContent(ref type, json);
                    }
                    else
                    {
                        stringContent = String.Format("Document {0} not found. ", identifier);
                        type = "text/plain";
                    }
                }
                else
                {
                    stringContent = "Storage not accessible ";
                    type = "text/plain";
                }
                contResult = base.Content(stringContent, type, System.Text.Encoding.UTF8);
            }
            catch (Exception ex)
            {
                _logger.LogError("DocumentController.Get(identifier) rise a error : " + Environment.NewLine + ex.Message);
                contResult = base.Content("Ups. Unexpectet issue. Please contact administrator.", "text/plain", System.Text.Encoding.UTF8);
            }
            return contResult;
        }


        /// <summary>
        /// method convert json string to type specified in type parameter
        /// </summary>
        /// <param name="type">MIME type format</param>
        /// <param name="json">json string</param>
        /// <returns></returns>
        string GetRightContent(ref string type, string json)
        {
            string stringContent = string.Empty;

            switch (type)
            {
                case "application/json":
                case "text/json":
                case "*/*":
                    stringContent = json;
                    type = "application/json";
                    break;
                case "application/xml":
                case "text/xml":
                    XNode node = JsonConvert.DeserializeXNode(json, "Document");
                    stringContent = node.ToString();
                    break;
                case "application/x-msgpack":
                    byte[] msgPackDoc = MessagePackSerializer.ConvertFromJson(json);
                    stringContent = System.Text.Encoding.UTF8.GetString(msgPackDoc);
                    break;
                //case "newFormat":   // example for adding for new content type
                //    stringContent = conversionToNewFormat(json);
                //    type = "newContentType"; // add it if is necessery change type
                default:
                    stringContent = "Not suportet Content Type : " + type;
                    type = "text/plain";
                    break;
            }

            return stringContent;
        }

        /// <summary>
        /// method save document to storage
        /// </summary>
        /// <param name="document">document need to hava ID</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ContentResult> PostAsync(Document document)
        {
            ContentResult contResult;
            string stringContent = string.Empty;
            int statusCode = 200;
            try
            {
                if (document != null && document.id != string.Empty)
                {
                    if (storage != null)
                    {
                        // store method call here
                        if (await storage.SaveAsync(document))
                        {
                            stringContent = "Document saved.";
                            statusCode = 201;
                        }
                        else
                        {
                            stringContent = "Ups. Unexpectet problem when saving the document. Please contact administrator.";
                        }
                    }
                    else
                    {
                        stringContent = "Storage not accessible ";
                    }
                }
                else
                {
                    stringContent = "Document does not contain an ID. Please add ID to document and try again";
                }
                contResult = base.Content(stringContent, "text/plain", System.Text.Encoding.UTF8);
                contResult.StatusCode = statusCode;
            }
            catch (Exception ex)
            {
                // TO DO  --> log error somewhere
                _logger.LogError("DocumentController.Post(document) rise a error : " + Environment.NewLine + ex.Message);
                contResult = base.Content("Ups. Unexpectet issue. Please contact administrator.", "text/plain", System.Text.Encoding.UTF8);
            }
            return contResult;
        }

        /// <summary>
        /// method edit stored document. Document must content ID for identification and some of his variable.
        /// document does not have to be complete
        /// </summary>
        /// <param name="document">Document must content ID</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ContentResult> PutAsync(Document document)
        {
            ContentResult contResult;
            string stringContent = string.Empty;
            try
            {
                if (document != null && document.id != string.Empty)
                {
                    if (storage != null)
                    {
                        // store method call here
                        if (await storage.EditAsync(document))
                        {
                            stringContent = "Document saved.";
                        }
                        else
                        {
                            stringContent = "Ups. Unexpectet problem when editing the document. Please contact administrator.";
                        }
                    }
                    else
                    {
                        stringContent = "Storage not accessible ";
                    }
                }
                else
                {
                    stringContent = "Document does not contain an ID. Please add ID to document and try again";
                }
                contResult = base.Content(stringContent, "text/plain", System.Text.Encoding.UTF8);
            }
            catch (Exception ex)
            {
                _logger.LogError("DocumentController.Put(document) rise a error : " + Environment.NewLine + ex.Message);
                contResult = base.Content("Ups. Unexpectet issue. Please contact administrator.", "text/plain", System.Text.Encoding.UTF8);
            }
            return contResult;
        }
    }
}
