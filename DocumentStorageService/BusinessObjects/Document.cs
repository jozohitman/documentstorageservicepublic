﻿using System.Collections.Generic;


namespace DocumentStorageService
{
    /// <summary>
    /// bussines object document class
    /// </summary>
    public class Document 
    {
        /// <summary>
        /// document ID
        /// </summary>
        public string id { get; set; } = string.Empty;

        /// <summary>
        /// collection of tags strings
        /// </summary>
        public List<string> tags { get; set; } = new List<string>();

        /// <summary>
        /// should content any json converted object
        /// </summary>
        public dynamic data { get; set; }
        
    }
}
