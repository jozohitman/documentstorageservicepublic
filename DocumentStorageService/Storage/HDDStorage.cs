﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DocumentStorageService
{
    public class HDDStorage : IStorage
    {
        // path to storage location
        string globalPath = "D:\\";
        string documentName = "{0}.json";
        // wrapper for IO operations
        private readonly  IFileSys FileSys;

        public HDDStorage()
        {
            FileSys = new FileSys();
        }


        public HDDStorage(IFileSys fileSys)
        {
            FileSys = fileSys;
        }

        /// <summary>
        /// method save document as text to HDD storage
        /// </summary>
        /// <param name="document">Document must content ID</param>
        /// <returns></returns>
        public async Task<bool> SaveAsync(Document document)
        {
            string path =  string.Format(globalPath + documentName, document.id);
            string fileContent = JsonConvert.SerializeObject(document);
            try
            {   //in this logic --> if file exist we replace it
                if (FileSys.DirectoryExists(globalPath))
                {   
                    await FileSys.WriteAllTextAsync(path, fileContent, System.Text.Encoding.UTF8);
                }
                else
                {
                    // log directory not exist
                    return false;
                }
            }
            catch (Exception ex)
            {
                // TO DO -> log exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method load document as text from HDD storage
        /// </summary>
        /// <param name="id">ID of requested document</param>
        /// <returns></returns>
        public async Task<string> LoadAsync(string id)
        {
            string path = string.Format(globalPath + documentName, id);
            string json = string.Empty;
            try
            {
                if (FileSys.FileExists(path))
                {
                    json = await FileSys.ReadAllTextAsync(path, System.Text.Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                // TO DO -> log exception
                return string.Empty;
            }

            return json;
        }

        /// <summary>
        /// method to edit stored document piece by piece
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public async Task<bool> EditAsync(Document document)
        {
            bool response = true;
            try
            {
                if (FileSys.DirectoryExists(globalPath))
                {
                    Document loadedDoc = JsonConvert.DeserializeObject<Document>(await LoadAsync(document.id));
                    if (loadedDoc != null)
                    {
                        if (document.tags.Count != 0)
                        {
                            loadedDoc.tags = document.tags;
                        }
                        if (document.data != null)
                        {
                            loadedDoc.data = document.data;
                        }
                        string path = string.Format(globalPath + documentName, loadedDoc.id);

                        await FileSys.WriteAllTextAsync(path, JsonConvert.SerializeObject(loadedDoc), System.Text.Encoding.UTF8);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    // log directory not exist
                    return false;
                }
            }
            catch(Exception ex)
            {
                // TO DO -> log exception
                return false;
            }
            return response;
        }
    }

    /// <summary>
    /// wrapper class IO (File,Directory) operations, used for bether testing
    /// </summary>
    public class FileSys : IFileSys
    {
        public bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public bool DirectoryExists(string path)
        {
            return Directory.Exists(path);
        }

        public async Task<string> ReadAllTextAsync(string path, System.Text.Encoding encoding)
        {
            return await File.ReadAllTextAsync(path, encoding);
        }

        public async Task WriteAllTextAsync(string path, string text ,System.Text.Encoding encoding)
        {
             await File.WriteAllTextAsync(path, text, encoding);
        }
    }

    /// <summary>
    /// interface for wrapper class
    /// </summary>
    public interface IFileSys
    {
        public bool FileExists(string path);

        public bool DirectoryExists(string path);

        public Task<string> ReadAllTextAsync(string path, System.Text.Encoding encoding);

        public Task WriteAllTextAsync(string path, string text, System.Text.Encoding encoding);
    }
}
