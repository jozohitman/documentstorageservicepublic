﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentStorageService
{
    /// <summary>
    /// interface for storing document
    /// </summary>
    public interface IStorage
    {
        public  Task<bool> SaveAsync(Document document);

        public Task<string> LoadAsync(string id);

        public Task<bool> EditAsync(Document document);

    }
}
