using DocumentStorageService;
using DocumentStorageService.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace DocumentStorageServiceTest
{
    public class DocumentControllerTests
    {
        private readonly DocumentsController docController;
        private readonly Mock<IStorage> _storageMock = new Mock<IStorage>();
        private readonly Mock<ILogger<DocumentsController>> _loggerMock = new Mock<ILogger<DocumentsController>>();

        public DocumentControllerTests()
        {
            docController = new DocumentsController(_loggerMock.Object, _storageMock.Object);
        }

        [Fact]
        public async Task GetAsync_ShouldReturnDocument_WhenDocumetnExists()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "application/json";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";
            string loadedJson = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";

            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(loadedJson);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert
            Document returnDoc = JsonConvert.DeserializeObject<Document>(contentResult.Content);
            Assert.Equal(docId,returnDoc.id );
        }

        [Fact]
        public async Task GetAsync_ShouldReturnJsonDocument_WhenAcceptJson()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "application/json";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";
            string loadedJson = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ]
                                }";

            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(loadedJson);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert
            
            Assert.Equal(httpContext.Request.Headers["Accept"].ToString(), contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task GetAsync_ShouldReturnXMLDocument_WhenAcceptXML()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "application/xml";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";
            string loadedJson = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ]
                                }";

            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(loadedJson);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert

            Assert.Equal(httpContext.Request.Headers["Accept"].ToString(), contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task GetAsync_ShouldReturnNotSupportedMessage_WhenAcceptHTML()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "text/html";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";
            string loadedJson = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ]
                                }";

            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(loadedJson);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert

            Assert.Equal(contentResult.Content, "Not suportet Content Type : " + httpContext.Request.Headers["Accept"]);
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task GetAsync_ShouldReturnMsgpackDocument_WhenAcceptMsgpack()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "application/x-msgpack";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";
            string loadedJson = "{\"id\": \"testDoc-1\",\"tags\": [\"firstNode\",\"secondNode\"],\"data\": {\"message\": \"Hi\",\"Rok\": \"2022\",\"some\": \"data\",\"optional\": \"fields\"} }";

            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(loadedJson);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert

            Assert.Equal(httpContext.Request.Headers["Accept"].ToString(), contentResult.ContentType.Split(';')[0]);
        }
    
        [Fact]
        public async Task GetAsync_ShouldReturnNotFoundMeesage_WhenDocumentNotExist()
        {
            //Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["Accept"] = "application/json";
            docController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = httpContext
            };

            string docId = "testDoc-1";


            _storageMock.Setup(s => s.LoadAsync(docId))
                .ReturnsAsync(string.Empty);

            //Act
            var contentResult = await docController.GetAsync(docId);

            //Assert
            Assert.Equal(contentResult.Content, String.Format("Document {0} not found. ", docId));
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task PostAsync_ShouldReturnSavedMessage_WhenDocumentWasSaved()
        {     
            //Arrange
            
            string docId = "testDoc-1";
            string json = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";
            Document doc = JsonConvert.DeserializeObject<Document>(json);
            _storageMock.Setup(s => s.SaveAsync(doc))
                .ReturnsAsync(true);

            //Act
            var contentResult = await docController.PostAsync(doc);

            //Assert
            Assert.Equal(201, contentResult.StatusCode);

            Assert.Equal("Document saved.", contentResult.Content);
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task PostAsync_ShouldReturnDoesnotHaveIdMessage_WhenDocumentNotContentId()
        {
            //Arrange

            string docId = "testDoc-1";
            string json = @"{
                            
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";
            Document doc = JsonConvert.DeserializeObject<Document>(json);
            _storageMock.Setup(s => s.SaveAsync(doc))
                .ReturnsAsync(true);

            //Act
            var contentResult = await docController.PostAsync(doc);

            //Assert
            Assert.Equal(200, contentResult.StatusCode);

            Assert.Equal("Document does not contain an ID. Please add ID to document and try again", contentResult.Content);
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task PutAsync_ShouldReturnSavedMessage_WhenDocumentWasEdited()
        {
            //Arrange
           
            string json = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";
            Document doc = JsonConvert.DeserializeObject<Document>(json);
            _storageMock.Setup(s => s.EditAsync(doc))
                .ReturnsAsync(true);

            //Act
            var contentResult = await docController.PutAsync(doc);

            //Assert
            Assert.Equal("Document saved.", contentResult.Content);
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }

        [Fact]
        public async Task PutAsync_ShouldReturnDoesnotHaveIdMessage_WhenDocumentNotContentId()
        {
            //Arrange

            string docId = "testDoc-1";
            string json = @"{
                            
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";
            Document doc = JsonConvert.DeserializeObject<Document>(json);
            _storageMock.Setup(s => s.EditAsync(doc))
                .ReturnsAsync(true);

            //Act
            var contentResult = await docController.PutAsync(doc);

            //Assert

            Assert.Equal("Document does not contain an ID. Please add ID to document and try again", contentResult.Content);
            Assert.Equal("text/plain", contentResult.ContentType.Split(';')[0]);
        }
    }
}
