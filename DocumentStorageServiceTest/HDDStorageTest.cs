﻿using DocumentStorageService;
using Moq;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace DocumentStorageServiceTest
{
    public class HDDStorageTest
    {
        private readonly Mock<IFileSys> _fileSysMock = new Mock<IFileSys>();

        private HDDStorage storage;
        string loadedJson;

        public HDDStorageTest()
        {
            storage = new HDDStorage(_fileSysMock.Object);
            _fileSysMock.Setup(f => f.WriteAllTextAsync(It.IsAny<String>(), It.IsAny<String>(), System.Text.Encoding.UTF8)).Verifiable();
            loadedJson = @"{
                            'id': 'testDoc-1',
                            'tags': [
                                'firstNode',
                                'secondNode'
                            ],
                            'data': {
                                        'message': 'Hi',
                                'Rok': '2022',
                                'some': 'data',
                                'optional': 'fields'
                            }
                                }";
            _fileSysMock.Setup(f => f.ReadAllTextAsync(It.IsAny<String>(), System.Text.Encoding.UTF8)).ReturnsAsync(loadedJson);
        }

        [Fact]
        public async Task LoadAsync_ShouldReturnJsonText_WhenFileAndDirectoryExists()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(true);
            _fileSysMock.Setup(f => f.FileExists(It.IsAny<String>())).Returns(true);
            string docId = "testDoc-1";
            //Act
            var result = await storage.LoadAsync(docId);

            //Assert
            Document returnDoc = JsonConvert.DeserializeObject<Document>(result);
            Assert.Equal(docId, returnDoc.id);

            _fileSysMock.Verify(f => f.ReadAllTextAsync(It.IsAny<String>(), System.Text.Encoding.UTF8), Times.Once);
        }

        [Fact]
        public async Task LoadAsync_ShouldReturnStringEmpty_WhenFileNotExists()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(true);
            _fileSysMock.Setup(f => f.FileExists(It.IsAny<String>())).Returns(false);
            string docId = "testDoc-1";
            //Act
            var result = await storage.LoadAsync(docId);

            //Assert
            Assert.Equal(string.Empty, result);

            _fileSysMock.Verify(f => f.ReadAllTextAsync(It.IsAny<String>(), System.Text.Encoding.UTF8), Times.Never);
        }

        [Fact]
        public async Task SaveAsync_ShouldReturnTrue_WhenDirExistsAndFileWasWrited()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(true);
            
            string docId = "testDoc-1";
            Document doc = new Document() { id = docId };
            //Act
            var result = await storage.SaveAsync(doc);

            //Assert
            Assert.True(result);

            _fileSysMock.Verify(f => f.WriteAllTextAsync(It.IsAny<String>(), It.IsAny<String>(), System.Text.Encoding.UTF8),
                Times.Once);
        }

        [Fact]
        public async Task SaveAsync_ShouldReturnFalse_WhenDirNotExists()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(false);

            string docId = "testDoc-1";
            Document doc = new Document() { id = docId };
            //Act
            var result = await storage.SaveAsync(doc);

            //Assert
            Assert.False(result);

            _fileSysMock.Verify(f => f.WriteAllTextAsync(It.IsAny<String>(), It.IsAny<String>(), System.Text.Encoding.UTF8),
                Times.Never);
        }

        [Fact]
        public async Task EditAsync_ShouldReturnFalse_WhenDirNotExists()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(false);

            string docId = "testDoc-1";
            Document doc = new Document() { id = docId };
            //Act
            var result = await storage.SaveAsync(doc);

            //Assert
            Assert.False(result);

            _fileSysMock.Verify(f => f.WriteAllTextAsync(It.IsAny<String>(), It.IsAny<String>(), System.Text.Encoding.UTF8),
                Times.Never);
        }

        [Fact]
        public async Task EditAsync_ShouldReturnTrue_WhenFileWasLoaded()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(true);
            _fileSysMock.Setup(f => f.FileExists(It.IsAny<String>())).Returns(true);
            string docId = "testDoc-1";

            Document Doc = JsonConvert.DeserializeObject<Document>(loadedJson);
            //Act
            var result = await storage.EditAsync(Doc);

            //Assert
            
            Assert.True(result);

            _fileSysMock.Verify(f => f.ReadAllTextAsync(It.IsAny<String>(), System.Text.Encoding.UTF8), Times.Once);
        }

        [Fact]
        public async Task EditAsync_ShouldReturnFalse_WhenFileWasNotLoaded()
        {
            //Arrange
            _fileSysMock.Setup(f => f.DirectoryExists(It.IsAny<String>())).Returns(true);
            _fileSysMock.Setup(f => f.FileExists(It.IsAny<String>())).Returns(true);
            string docId = "testDoc-1";
            // I substitute ReadAllText  to return string.empty from LoadAsync method
            _fileSysMock.Setup(f => f.ReadAllTextAsync(It.IsAny<String>(), System.Text.Encoding.UTF8)).ReturnsAsync(string.Empty);
            Document Doc = JsonConvert.DeserializeObject<Document>(loadedJson);
            //Act
            var result = await storage.EditAsync(Doc);

            //Assert

            Assert.False(result);

            _fileSysMock.Verify(f => f.WriteAllTextAsync(It.IsAny<String>(), It.IsAny<String>(), System.Text.Encoding.UTF8), Times.Never);
        }
    }
}
